# gitlab-ci
- https://docs.gitlab.com/ee/ci/introduction/index.html
- https://docs.gitlab.com/ee/ci/
- https://dev.to/zurihunter/beginner-friendly-introduction-to-gitlabcicd-4p5a

# gitlab runner
- https://about.gitlab.com/product/continuous-integration/

# install
- https://docs.gitlab.com/runner/install/linux-manually.html
- https://docs.gitlab.com/runner/install/linux-repository.html
- https://docs.gitlab.com/runner/register/index.html


# deploy
- https://gitlab.emottet.com/emilien/EmilienMottet-github-io

# environment
- https://docs.gitlab.com/ee/ci/environments.html
- https://docs.gitlab.com/ee/ci/variables/

# Jenkins Killer ?
- https://about.gitlab.com/devops-tools/jenkins-vs-gitlab.html
