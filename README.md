# GitelabCix

[![pipeline status](https://gitlab.com/EmilienMottet/gitelab_cix/badges/master/pipeline.svg)](https://gitlab.com/EmilienMottet/gitelab_cix/commits/master)
[![coverage report](https://gitlab.com/EmilienMottet/gitelab_cix/badges/master/coverage.svg)](https://gitlab.com/EmilienMottet/gitelab_cix/commits/master)


https://emilienmottet.gitlab.io/gitelab_cix

**description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `gitelab_cix` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:gitelab_cix, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/gitelab_cix](https://hexdocs.pm/gitelab_cix).

