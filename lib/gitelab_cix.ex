defmodule GitelabCix do
  @moduledoc """
  Documentation for GitelabCix.
  """

  @doc """
  Hello world.

  ## Examples

      iex> GitelabCix.hello()
      :world

  """
  def hello do
    :world
  end
end
