defmodule JSONAPI do
  use Ewebmachine.Builder.Handlers
  plug(:cors)
  plug(:add_handlers)

  content_types_provided(do: ["application/json": :to_json])
  defh(to_json, do: Poison.encode!(state[:json_obj]))

  defp cors(conn, _), do: put_resp_header(conn, "Access-Control-Allow-Origin", "*")
end

defmodule Server.EwebRouter do
  use Ewebmachine.Builder.Resources
  if Mix.env() == :dev, do: plug(Ewebmachine.Plug.Debug)
  plug(Plug.Static, at: "/public", from: :tutokbrwstack)
  resources_plugs(error_forwarding: "/error/:status", nomatch_404: true)

  resource "/api/me" do
    %{}
  after
    plug(JSONAPI)
    allowed_methods(do: ["GET"])

    resource_exists(
      do: pass(true, json_obj: %{first_name: "Admin", last_name: "Admin", id: "122"})
    )
  end
end
