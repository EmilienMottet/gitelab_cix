defmodule GitelabCix.MixProject do
  use Mix.Project

  def project do
    [
      app: :gitelab_cix,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {GitelabCix.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 1.1.2", override: true},
      {:plug, "~> 1.3.4"},
      {:poison, "~> 2.1.0"},
      {:ewebmachine, git: "https://github.com/kbrw/ewebmachine"},
      {:ex_doc, "~> 0.19", only: :dev, runtime: false}
    ]
  end
end
